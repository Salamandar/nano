#!/usr/bin/env bash

if [[ -d '.git' ]]; then
    echo "REVISION \"$(git describe --tags 2>/dev/null)\""
else
    echo 'NOTHING "from tarball"'
fi
