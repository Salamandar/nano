#!/bin/bash

tempbuilddir="$(mktemp -d)"

echo "Updating linguas…"
po/update_linguas.sh &&

git add po/*.po po/nano.pot &&
git commit -m "$(git log -1 --grep 'po: up' | grep o: | sed 's/^    //')" &&


echo "Configuring in ${tempbuilddir}…"

meson "${tempbuilddir}" --buildtype=release -Db_lto=true --prefix=/usr \
    -Dtiny=true &&

VERSION="$(ninja -C "${tempbuilddir}" print-version)"

echo "Building…"

ninja -C "${tempbuilddir}" &&
ninja -C "${tempbuilddir}" dist &&
ninja -C "${tempbuilddir}" doc/nano.pdf &&

ARCHIVE="${tempbuilddir}/meson-dist/nano-${VERSION}.tar.xz"
PDF_DOC="${tempbuilddir}/doc/nano.pdf"

gpg -a -b "${ARCHIVE}" &&
gpg --verify "${ARCHIVE}.asc" &&

git tag -u A0ACE884 -a "v$VERSION" -m "the nano $VERSION release" &&

scp "$ARCHIVE" bens@wh0rd.org:"nano-${VERSION}.tar.xz" &&
scp "$PDF_DOC" bens@wh0rd.org:nano.pdf &&
scp doc/cheatsheet.html bens@wh0rd.org:cheatsheet.html &&

gnupload --to ftp.gnu.org:nano  "nano-$VERSION.tar".*z &&

echo "Tarballs have been rolled and uploaded."

rm -rf "${tempbuilddir}" &&
