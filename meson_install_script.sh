#!/usr/bin/env bash

# Create rnano by linking to nano
ln -fs "nano" "${MESON_INSTALL_DESTDIR_PREFIX}/bin/rnano"
